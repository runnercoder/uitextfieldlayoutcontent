//
//  DemoAppDelegate.h
//  UITextFieldLayoutContent
//
//  Created by starrykai on 09/07/2016.
//  Copyright (c) 2016 starrykai. All rights reserved.
//

@import UIKit;

@interface DemoAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
