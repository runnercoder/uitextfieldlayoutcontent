//
//  UITextField+AppTextFieldCategory.m
//  Tao
//
//  Created by 吴恺 on 15/6/3.
//  Copyright (c) 2015年 wukai. All rights reserved.
//

#import "UITextFieldLayoutContent.h"

@implementation UITextField (AppTextFieldCategory)

- (void)addLeftWhiteSpace:(CGFloat)leftSpace {
  CGRect frame = self.frame;
  frame.size.width = leftSpace;
  UIView *leftview = [[UIView alloc] initWithFrame:frame];
  self.leftViewMode = UITextFieldViewModeAlways;
  self.leftView = leftview;
}

- (void)addRightWhiteSpace:(CGFloat)rightSpace {
  CGRect frame = self.frame;
  frame.size.width = rightSpace;
  UIView *rightview = [[UIView alloc] initWithFrame:frame];
  self.rightViewMode = UITextFieldViewModeAlways;
  self.rightView = rightview;
}

- (void)addLeftWhiteSpace:(CGFloat)leftSpace
                      rightSpace:(CGFloat)rightSpace {
  [self addLeftWhiteSpace:leftSpace];
  [self addLeftWhiteSpace:rightSpace];
}

- (void)passwordStyleShowButtonWithNormalStateImage:(NSString *)image1
                                          hiddenStateImage:(NSString *)image2 {
  UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
  [button setImage:[UIImage imageNamed:image1] forState:UIControlStateSelected];
  [button setImage:[UIImage imageNamed:image2] forState:UIControlStateNormal];
  [button addTarget:self
                action:@selector(buttonPressed:)
      forControlEvents:UIControlEventTouchUpInside];
  [button setTitle:@" " forState:UIControlStateNormal];
  CGSize imageSize = [UIImage imageNamed:image1].size;
  CGRect frame = CGRectMake(0, 0, imageSize.width + 6, imageSize.height);
  button.frame = frame;
  self.rightViewMode = UITextFieldViewModeAlways;
  self.rightView = button;
}

- (void)buttonPressed:(UIButton *)sender {
  BOOL isfirstRespond = [self isFirstResponder];
  self.enabled = NO;
  self.secureTextEntry = !self.isSecureTextEntry;
  self.enabled = YES;
  sender.selected = !self.isSecureTextEntry;
  if (isfirstRespond || self.text.length == 0) {
    //    如果之前是第一响应 或 内容为空，则自动激活键盘
    [self becomeFirstResponder];
  }
}

@end
