//
//  UITextFieldLayoutContent.h
//  Tao
//
//  Created by 吴恺 on 15/6/3.
//  Copyright (c) 2015年 wukai. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UITextField (AppTextFieldCategory)

// 修改TextField左侧空白位置
- (void)addLeftWhiteSpace:(CGFloat)leftSpace;
// 修改TextField右侧空白位置
- (void)addRightWhiteSpace:(CGFloat)rightSpace;
// 修改TextField左侧和右侧空白位置
- (void)addLeftWhiteSpace:(CGFloat)leftSpace
                      rightSpace:(CGFloat)rightSpace;
// 登陆密码的显示和隐藏
- (void)passwordStyleShowButtonWithNormalStateImage:(NSString *)image1
                                          hiddenStateImage:(NSString *)image2;

@end
