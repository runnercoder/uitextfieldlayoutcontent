#
# Be sure to run `pod lib lint UITextFieldLayoutContent.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'UITextFieldLayoutContent'
  s.version          = '0.0.3'
  s.summary          = '更改UITextField默认内边距'

  s.description      = <<-DESC
1、设置UITextField的左右内边距，实现修改光标起始位置。
2、仿支付宝显示密码按钮
                       DESC

  s.homepage         = 'https://bitbucket.org/runnercoder/uitextfieldlayoutcontent'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'starrykai' => 'starryskykai@gmail.com' }
  s.source           = { :git => 'https://runnercoder@bitbucket.org/runnercoder/uitextfieldlayoutcontent.git', :tag => s.version.to_s }
  s.ios.deployment_target = '8.0'
  s.source_files = 'UITextFieldLayoutContent/Classes/*.{h,m}'
  s.frameworks = 'UIKit'

end
